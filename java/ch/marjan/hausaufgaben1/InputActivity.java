package ch.marjan.hausaufgaben1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InputActivity extends AppCompatActivity {
    protected static final String KEY_1 = "key1";

    EditText htmlInput;
    Button btnSendHtml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        htmlInput = (EditText) findViewById(R.id.inputActivity_txtInputHtml);
        btnSendHtml = (Button) findViewById(R.id.inputActivity_btnSend);


        btnSendHtml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String htmlInputContent = htmlInput.getText().toString().trim();

                if (!htmlInputContent.isEmpty()) {
                    Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                    intent.putExtra(KEY_1, htmlInputContent);
                    startActivity(intent);
                } else {
                    Toast toast = Toast.makeText(v.getContext(), "empty html content!", Toast.LENGTH_LONG);
                    toast.show();
                }

            }
        });


    }
}
